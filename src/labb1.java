import java.util.Scanner;

//Christian Heina 940323-3274

public class labb1 {
	
	static Scanner sc = new Scanner(System.in);

	public static void main(String[] args) {
		menu();
	}
	
	// Menu for the user to decide what to do, using switch
	public static void menu(){
		sc = new Scanner(System.in);
		System.out.println("Vad vill du g�ra?\n1. Addera tv� tal\n2. R�kna bokst�ver i en str�ng" +
				"\n3. Spegelv�nd en str�ng\n4. Summera alla tal i en str�ng\ne. Avsluta");
		
		char choice = sc.nextLine().charAt(0);
		switch (choice){
		case '1':
			add();
			break;
		case '2':
			letters();
			break;
		case '3':
			mirror();
			break;
		case '4':
			addString();
			break;
		case 'e':
			quit();
			break;
		default:
			System.out.println("Ej m�jligt val, f�rs�k igen \n");
			menu();
			break;
		}
	}
	
	// Lets the user input the numbers, adds them and prints them
	public static void add (){
		System.out.print("F�rsta talet: ");
		int num1 = sc.nextInt();
		System.out.print("Andra talet: ");
		int num2 = sc.nextInt();
		System.out.println("Summan �r " + (num1+num2) + "\n");
		menu();
	}
	
	// asks for string and letter, compare the individual char in the string with the letter (also char)
	// Shows the user the result
	public static void letters (){
		char c;
		int count = 0;
		String word;
		
		System.out.print("Ange ordet du vill leta i: ");
		word = sc.nextLine();
		System.out.print("Ange bokstaven du vill leta efter: ");
		c = sc.next().charAt(0);
		
		for (int i = 0; i<word.length(); i++){
			if (c == word.charAt(i)){
				count++;
			}
		}
		System.out.println("Det finns " + count + " " + c + " i ordet " + word + "\n");
		menu();
	}
	
	// Lets the user input a string then mirrors it by taking the characters from the back of the string and 
	// printing them one by one
	public static void mirror (){
		String word;
		
		System.out.print("Ge mig en str�ng: ");
		word = sc.nextLine();
		
		for (int i=word.length()-1;i>=0;i--){
			System.out.print(word.charAt(i));
		}
		
		System.out.println("\n");
		menu();
	}
	
	// Takes a string with numbers decided by the user and adds them only if the entire string is made up
	// of numbers otherwise it gives the user an error message and moves on
	public static void addString (){
		String numbers;
		int sum = 0;
		boolean endMessage = true;
		
		System.out.print("Ge mig en str�ng av siffror: ");
		numbers = sc.nextLine();
		for(int i=0;i<numbers.length();i++){
			if(isDigit(numbers.charAt(i))){
				sum += numbers.charAt(i)-48;
			}else{
				System.out.println("Inneh�ller mer �n siffror\n");
				endMessage = false;
				break;
			}
		}
		
		if (endMessage)
			System.out.println("Summan av siffrorna �r " + sum + "\n");
		
		menu();
	}

	public static void quit() {
		System.out.print("Hejd�");
	}
	
	// asks for a char and checks if the char is a number or not, returning a boolean
	public static boolean isDigit(char c){
		boolean isNumber = true;
		if (c-48 < 0 || c-48 >9){
			isNumber = false;
		}
		return isNumber;	
	}

}
